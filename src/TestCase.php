<?php
namespace Sebwite\Testbench;

use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Sebwite\Testing\Laravel\Traits\BindingGetters;
use Sebwite\Testing\Laravel\Traits\TestHelpers;

/**
 * This is the AbstractTestCase.
 *
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
abstract class TestCase extends OrchestraTestCase
{

    use TestHelpers, BindingGetters;

    protected function isProviderRegisterd($providerClass)
    {
        return in_array($providerClass, $this->app->getLoadedProviders(), true);
    }

    # Beverage service provider

    protected function getSupportServiceProvider()
    {
        return \Sebwite\Support\SupportServiceProvider::class;
    }

    protected function registerSupportServiceProvider()
    {
        return $this->app->register($this->getSupportServiceProvider());
    }



    # Service provider for the package

    /**
     * getServiceProviderClass
     *
     * @return \Sebwite\Support\ServiceProvider
     */
    abstract protected function getServiceProviderClass();

    /**
     * registerServiceProvider
     *
     * @return \Sebwite\Support\ServiceProvider
     */
    protected function registerServiceProvider()
    {
        $this->registerSupportServiceProvider();
        $class = $this->getServiceProviderClass();
        /** @var \Sebwite\Support\ServiceProvider $provider */
        $this->app->register($class);
    }

    /**
     * getProvider
     *
     * @return \Sebwite\Support\ServiceProvider
     */
    protected function getServiceProvider()
    {
        $this->registerServiceProvider();
        return $this->app->getProvider($this->getServiceProviderClass());
    }


    # Package helpers

    abstract protected function getPackageRootPath();

    protected function getPackagePath($path = null)
    {
        return is_null($path) ? $this->getPackageRootPath() : $this->getPackageRootPath() . DIRECTORY_SEPARATOR . $path;
    }

    protected function getPackageConfig()
    {
        return $this->getServiceProvider()->pathsToPublish(null, 'config');
    }

    protected function getPackageFile($path)
    {
        $this->getFiles()->get($this->getPackagePath($path));
    }



    # Misc

    /**
     * Executes a kernel command
     *
     * @param string $command
     */
    protected function command($command)
    {
        $this->getKernel()->call($command);
    }

    /**
     * Setup the application environment.
     *
     * @param \Illuminate\Foundation\Application $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $config = $app->make('config');
        $config->set('cache.driver', 'array');
        $config->set('app.key', 'sG7qHHCc0jAseXbQx5BEv8DiZn4x7p4C');
        $config->set('database.default', 'sqlite');
        $config->set(
            'database.connections.sqlite',
            [
                'driver'   => 'sqlite',
                'database' => ':memory:',
                'prefix'   => '',
            ]
        );
        $config->set('mail.driver', 'log');
        $config->set('session.driver', 'array');


        #$app->call('command.migrate');
        # $app->make('mailer')->pretend(true);
    }

    /**
     * createTestingEnv
     *
     * @param $dir
     */
    protected function createTestingEnv($dir)
    {
        /** @var \Illuminate\Foundation\Application $app */
        $app     = $this->app;
        $baseDir = base_path($dir);

        $app->bind('path.public', function () use ($baseDir) {

            return $baseDir . '/public';
        });

        $app->bind('path.base', function () use ($baseDir) {

            return $baseDir;
        });


        $app->bind('path.storage', function () use ($baseDir) {

            return $baseDir . '/storage';
        });

        $app->flush();
    }

    protected function writeToCLI($var)
    {
        fwrite(STDERR, print_r($var, true));
    }
}
